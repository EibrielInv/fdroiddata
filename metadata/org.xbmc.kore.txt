Categories:Multimedia
License:Apache2
Web Site:http://kodi.tv
Source Code:https://github.com/xbmc/Kore
Issue Tracker:https://github.com/xbmc/Kore/issues

Auto Name:Kore
Summary:Remote control for Kodi (XBMC)
Description:
Simple remote control for [http://kodi.tv Kodi]. In-app purchases
(e.g. for themes) are broken, since they require proprietary play-
services to be installed on the device and a special API key, which
FDroid cannot include.

[https://github.com/xbmc/Kore/blob/HEAD/CHANGELOG.md Changelog]
.

Repo Type:git
Repo:https://github.com/xbmc/Kore

Build:1.2.1,9
    commit=v1.2.1
    subdir=app
    gradle=yes
    prebuild=echo "IAP_KEY 'foo'" >> ../gradle.properties

Build:1.3.0,10
    commit=v1.3.0
    subdir=app
    gradle=yes
    prebuild=echo "IAP_KEY 'foo'" >> ../gradle.properties

Build:1.4.0,11
    commit=v1.4.0
    subdir=app
    gradle=yes
    prebuild=echo "IAP_KEY 'foo'" >> ../gradle.properties

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.4.0
Current Version Code:11

