Categories:Navigation
License:Apache2
Web Site:https://github.com/n76/Local-GSM-Backend/blob/HEAD/README.md
Source Code:https://github.com/n76/Local-GSM-Backend
Issue Tracker:https://github.com/n76/Local-GSM-Backend/issues

Name:LocalGsmNlpBackend
Auto Name:GSM Location Backend
Summary:UnifiedNlp location provider (local GSM database)
Description:
[[com.google.android.gms]] backend that uses local GSM data to resolve user
location.

A facility in the setting menu allows you to create a database using data from
[http://opencellid.org OpenCellId] and/or [https://location.services.mozilla.com Mozilla Location Services]
CSV files. Alternatively, the on-phone database can be generated in advance via
the [https://github.com/n76/lacells-creator lacells-creator] scripts that gather
tower information from those two sources, too.

This backend performs no network data. All data acquired by the phone stays on
the phone and no queries are made to a centralized AP location provider.
.

Repo Type:git
Repo:https://github.com/n76/Local-GSM-Backend

Build:0.5.4,9
    commit=v0.5.4
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Build:0.5.6,11
    commit=v0.5.6
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Build:0.5.8,13
    commit=v0.5.8
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Build:0.5.10,15
    commit=v0.5.10
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Build:0.5.11,16
    commit=v0.5.11
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Build:0.6.0,17
    commit=v0.6.0
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Build:0.6.1,18
    commit=v0.6.1
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Build:0.6.4,19
    commit=v0.6.4
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Build:0.7.0,20
    commit=v0.7.0
    srclibs=1:UnifiedNlpApi@v1.0.1
    rm=libs/UnifiedNlpApi.jar
    target=android-19

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.7.0
Current Version Code:20

